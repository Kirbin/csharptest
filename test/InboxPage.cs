﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    public class InboxPage
    {
        public InboxPage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "Написать письмо")]
        public IWebElement ComposeButton { get; set; }

        public void Compose()
        {
            ComposeButton.Click();
        }
    }
}
