﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    public class Program
    {
        private static string username = "your_email";
        private static string password = "password";
        private static string mailTo = "silent_nik@mail.ru";
        private static string mailSubj = "Test";
        private static string mailText = "SADQDqwdq wASD Qwdasd AWQd sqdq wOh iSDUfhUDhaiusfu aiwbd iNASIDu iawudqwd";

        static void Main(string[] args)
        {
            IWebDriver driver = new FirefoxDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.Navigate().GoToUrl("https://mail.ru/");

            LoginPage loginPage = new LoginPage(driver);
            loginPage.Login(username, password);

            InboxPage inboxPage = new InboxPage(driver);
            inboxPage.Compose();

            ComposeMessagePage compose = new ComposeMessagePage(driver);
            compose.ComposeAndSend(driver, mailTo, mailSubj, mailText);

            
        }
    }
}
