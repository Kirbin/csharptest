﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    public class ComposeMessagePage
    {

        public ComposeMessagePage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);          
        }

        [FindsBy(How = How.CssSelector, Using = "textarea[class*='js-input compose__labels__input']")]
        public IWebElement FieldTo { get; set; }

        [FindsBy(How = How.CssSelector, Using = "input[name*='Subject']")]
        public IWebElement FieldSubj { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='is-submit_empty_message_in']//button[@type='submit']")]
        public IWebElement SendButton { get; set; }
        
        public void ComposeAndSend(IWebDriver driver, string to, string subj, string text)
        {

            FieldTo.SendKeys(to);
            FieldSubj.Click();
            FieldSubj.SendKeys(subj);

            var textFrame = driver.FindElement(By.XPath("//iframe[contains(@id,'compose')]"));
            driver.SwitchTo().Frame(textFrame);
            var textArea = driver.FindElement(By.Id("tinymce"));
            textArea.Click();
            textArea.SendKeys(text);
            driver.SwitchTo().ParentFrame();


            driver.SwitchTo().ActiveElement().SendKeys(Keys.Control + Keys.Enter);
            
        }
    }
}
