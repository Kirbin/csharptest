﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace test
{
    public class LoginPage
    {
        public LoginPage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }



        [FindsBy(How = How.Id, Using = "mailbox:login")]
        public IWebElement username { get; set; }

        [FindsBy(How = How.Id, Using = "mailbox:password")]
        public IWebElement password { get; set; }

        [FindsBy(How = How.Id, Using = "mailbox:submit")]
        public IWebElement loginButton { get; set; }

        private IWebDriver driver;


        public void Login(string username, string password)
        {
            this.username.SendKeys(username);
            this.password.SendKeys(password);
            this.loginButton.Click();
        }
    }
}
